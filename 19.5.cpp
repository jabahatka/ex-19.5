

#include <iostream>


class Animal
{
public:

    std::string Text;

    virtual int Voice()
    {
        std::cout << "basic srting\n";

        return 0;
    }

};

class Snake : public Animal
{
public:

    int Voice()
    {
        std::cout << "Shhhhhhh!\n";

        return 0;
    }
};

class Dog : public Animal
{
public:

    int Voice()
    {
        std::cout << "Woof!\n";

        return 0;
    };
};

class Cat : public Animal
{
public:

    int Voice()
    {
        std::cout << "Meow!\n";

        return 0;
    }
};

int main()
{
    Animal* Array[3];

    Snake A;
    Dog B;
    Cat C;

    Array[0] = &A;
    Array[1] = &B;
    Array[2] = &C;

    for (int i = 0; i < 3; i++)
    {
        std::cout << "i = " << i << "; ";
        Array[i] -> Voice();
        std::cout << "\n";
    }
}
